<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Entity\Person;
use App\Tests\Support\FunctionalTester;
use Codeception\Example;
use Symfony\Component\HttpFoundation\Response;

class CreatePersonControllerCest
{
    const URL = '/person';

    /**
     * @dataProvider invalidRequestProvider
     */
    public function invalidRequestTest(FunctionalTester $I, Example $data)
    {
        // act
        $I->sendPost(self::URL, $data['request']);

        // assert
        $I->seeResponseCodeIs(Response::HTTP_BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->assertSame($data['response'], $I->grabResponseJson());
    }

    public function successCreateTest(FunctionalTester $I)
    {
        // arrange
        $apartmentId = $I->createApartment()->getId();
        $personalId = $I->getFaker()->uuid();

        // act
        $I->sendPost(
            self::URL,
            [
                'apartmentId' => $apartmentId,
                'firstName' => $I->getFaker()->firstName(),
                'lastName' => $I->getFaker()->lastName(),
                'birthdate' => (new \DateTimeImmutable())->format('Y-m-d'),
                'personalId' => $personalId,
            ]
        );

        // assert
        $I->seeResponseCodeIs(Response::HTTP_NO_CONTENT);
        $I->seeInRepository(
            Person::class,
            ['apartment' => $apartmentId, 'personalId' => $personalId]
        );
    }

    protected function invalidRequestProvider(): \Generator
    {
        $apartmentId = '17d2135a-dd80-4886-9513-05300fc4e598';

        yield 'empty' => [
            'request' => [],
            'response' => [
                'apartmentId' => [
                    'This value should not be blank.',
                ],
                'firstName' => [
                    'This value should not be blank.',
                ],
                'lastName' => [
                    'This value should not be blank.',
                ],
                'birthdate' => [
                    'This value should not be blank.',
                ],
                'personalId' => [
                    'This value should not be blank.',
                ],
            ],
        ];
        yield 'not found apartmentId' => [
            'request' => [
                'apartmentId' => $apartmentId,
                'firstName' => 'John',
                'lastName' => 'Doe',
                'birthdate' => (new \DateTimeImmutable())->format('Y-m-d'),
                'personalId' => 'personalId',
            ],
            'response' => "Entity not found: Apartment::{$apartmentId}",
        ];
    }
}
