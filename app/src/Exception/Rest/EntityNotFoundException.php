<?php

declare(strict_types=1);

namespace App\Exception\Rest;

use App\Exception\ApplicationException;

class EntityNotFoundException extends ApplicationException implements PublicErrorMessageInterface
{
    public function __construct(
        private readonly string $entityClass,
        private readonly string $entityId,
    ) {
        parent::__construct();
    }

    public function getPublicMessage(): string
    {
        return sprintf(
            'Entity not found: %s::%s',
            $this->getShortClassName($this->entityClass),
            $this->entityId,
        );
    }

    private function getShortClassName(string $class): string
    {
        if ($pos = strrpos($class, '\\')) {
            return substr($class, $pos + 1);
        }

        return $class;
    }
}
