<?php

declare(strict_types=1);

namespace App\Service\CQRS;

interface QueryBusInterface
{
    /**
     * @return mixed
     */
    public function dispatch(QueryInterface $query);
}
