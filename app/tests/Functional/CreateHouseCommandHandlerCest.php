<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Command\CreateHouseCommand;
use App\CommandHandler\CreateHouseCommandHandler;
use App\Entity\House;
use App\Tests\Support\FunctionalTester;

class CreateHouseCommandHandlerCest
{
    private CreateHouseCommandHandler $handler;

    public function _before(FunctionalTester $I)
    {
        $this->handler = $I->grabService(CreateHouseCommandHandler::class);
    }

    public function invokeTest(FunctionalTester $I)
    {
        // arrange
        $number = '1';
        $street = 'first street';
        $command = new CreateHouseCommand($number, $street);

        // act
        ($this->handler)($command);

        // assert
        $I->seeInRepository(
            House::class,
            ['number' => $number, 'street' => $street]
        );
    }
}
