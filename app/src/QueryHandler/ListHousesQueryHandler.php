<?php

declare(strict_types=1);

namespace App\QueryHandler;

use App\Model\HouseModel;
use App\Query\ListHousesQuery;
use App\Repository\HouseRepository;
use App\Service\CQRS\QueryHandlerInterface;
use App\Service\Transformer\HousesTransformer;
use App\Service\Transformer\ListHousesQueryTransformer;

class ListHousesQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private readonly HouseRepository $repository,
        private readonly ListHousesQueryTransformer $queryTransformer,
        private readonly HousesTransformer $housesTransformer,
    ) {
    }

   /**
    * @return HouseModel[]
    */
    public function __invoke(ListHousesQuery $query): array
    {
        $houses = $this->repository->findByDto(
            $this->queryTransformer->toDto($query)
        );

        return $this->housesTransformer->listToModels($houses);
    }
}
