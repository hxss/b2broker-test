<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\Apartment;
use App\Exception\Rest\EntityNotFoundException;
use App\Factory\PersonFactory;
use App\Repository\ApartmentRepository;
use App\Tests\Support\UnitTester;
use Codeception\Stub;
use Faker\Factory;
use Faker\Generator;

class PersonFactoryCest
{
    private Generator $faker;

    public function _before(UnitTester $I)
    {
        $this->faker = Factory::create();
    }

    public function failFindApartmentTest(UnitTester $I)
    {
        // arrange
        $apartmentRepository = Stub::make(ApartmentRepository::class, ['find' => null]);
        $factory = new PersonFactory($apartmentRepository);
        $apartmentId = $this->faker->uuid();

        // act // assert
        $I->expectThrowable(
            new EntityNotFoundException(Apartment::class, $apartmentId),
            fn () => $factory->create(
                $apartmentId,
                'John',
                'Doe',
                new \DateTimeImmutable(),
                'qweqweqwe',
            )
        );
    }

    public function successCreateTest(UnitTester $I)
    {
        // arrange
        $apartment = new Apartment();
        $apartmentRepository = Stub::make(
            ApartmentRepository::class,
            ['find' => $apartment]
        );
        $factory = new PersonFactory($apartmentRepository);

        $firstName = $this->faker->firstName();
        $lastName = $this->faker->lastName();
        $birthdate = new \DateTimeImmutable();
        $personalId = $this->faker->uuid();

        // act
        $person = $factory->create(
            $this->faker->uuid(),
            $firstName,
            $lastName,
            $birthdate,
            $personalId,
        );

        // assert
        $I->assertRegExp('/^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/', $person->getId());
        $I->assertSame($firstName, $person->getFirstName());
        $I->assertSame($lastName, $person->getLastName());
        $I->assertSame($birthdate, $person->getBirthdate());
        $I->assertSame($personalId, $person->getPersonalId());
    }
}
