<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\Rest\FormValidationFailedException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AbstractBaseController extends AbstractController
{
    /**
     * @throws FormValidationFailedException
     */
    protected function validateRequestAndBuildAction(
        Request $request,
        string $formTypeClassName,
        string $dtoClassName,
    ): object {
        $form = $this->validateRequest($request, $formTypeClassName);

        return is_subclass_of($formTypeClassName, DataMapperInterface::class) ?
            $form->getData() :
            $this->arrayToObject($form->getData(), $dtoClassName);
    }

    /**
     * @throws FormValidationFailedException
     */
    protected function validateRequest(
        Request $request,
        string $formTypeClassName,
    ): FormInterface {
        $form = $this->createForm($formTypeClassName);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            throw new FormValidationFailedException($form);
        }

        return $form;
    }

    /**
     * @param mixed[] $data
     *
     * @throws ExceptionInterface
     */
    protected function arrayToObject(array $data, string $type): object
    {
        return (new Serializer([new ObjectNormalizer()]))->denormalize($data, $type, null);
    }
}
