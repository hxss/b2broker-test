<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Sequentially;

trait PaginationTypeTrait
{
    public function buildPaginationForm(FormBuilderInterface $builder): void
    {
        $builder
            ->add(
                'page',
                IntegerType::class,
                [
                    'required' => true,
                    'constraints' => new GreaterThan(0),
                    'empty_data' => '1',
                ]
            )
            ->add(
                'perPage',
                IntegerType::class,
                [
                    'required' => true,
                    'constraints' => new Sequentially([
                        new GreaterThan(0),
                        new LessThanOrEqual(20),
                    ]),
                    'empty_data' => '1',
                ]
            )
        ;
    }
}
