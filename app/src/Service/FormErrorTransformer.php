<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Form\FormInterface;

class FormErrorTransformer
{
    public function transform(FormInterface $form): array
    {
        $formErrors = $form->getErrors(true, true);
        $errorsArray = [];
        foreach ($formErrors as $error) {
            $errorsArray[$error->getOrigin()->getName()]
                [] = $error->getMessage();
        }

        return $errorsArray;
    }
}
