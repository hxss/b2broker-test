<?php

declare(strict_types=1);

namespace App\Subscriber;

use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class JsonRequestConverterSubscriber implements EventSubscriberInterface
{
    public function convertJsonStringToArray(ControllerEvent $event): void
    {
        $request = $event->getRequest();

        if ($request->getContentTypeFormat() !== 'json' || !$request->getContent()) {
            return;
        }

        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            throw new BadRequestHttpException('invalid json body: ' . $e->getMessage());
        }

        $request->request->replace(is_array($data) ? $data : []);
    }

    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'convertJsonStringToArray',
        ];
    }
}
