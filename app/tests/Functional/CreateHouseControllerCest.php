<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Entity\House;
use App\Tests\Support\FunctionalTester;
use Codeception\Example;
use Symfony\Component\HttpFoundation\Response;

class CreateHouseControllerCest
{
    const URL = '/house';

    /**
     * @dataProvider invalidRequestProvider
     */
    public function invalidRequestTest(FunctionalTester $I, Example $data)
    {
        // act
        $I->sendPost(self::URL, $data['request']);

        // assert
        $I->seeResponseCodeIs(Response::HTTP_BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->assertSame($data['response'], $I->grabResponseJson());
    }

    public function successCreateTest(FunctionalTester $I)
    {
        // arrange
        $number = '1';
        $street = 'first street';

        // act
        $I->sendPost(
            self::URL,
            [
                'number' => $number,
                'street' => $street,
            ]
        );

        // assert
        $I->seeResponseCodeIs(Response::HTTP_NO_CONTENT);
        $I->seeInRepository(
            House::class,
            ['number' => $number, 'street' => $street]
        );
    }

    protected function invalidRequestProvider(): \Generator
    {
        $number = '1';
        $street = 'first street';

        yield 'empty' => [
            'request' => [],
            'response' => [
                'number' => [
                    'This value should not be blank.',
                ],
                'street' => [
                    'This value should not be blank.',
                ],
            ],
        ];
        yield 'has not name' => [
            'request' => ['street' => $street],
            'response' => [
                'number' => [
                    'This value should not be blank.',
                ],
            ],
        ];
        yield 'has not street' => [
            'request' => ['number' => $number],
            'response' => [
                'street' => [
                    'This value should not be blank.',
                ],
            ],
        ];
    }
}
