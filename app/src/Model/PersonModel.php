<?php

declare(strict_types=1);

namespace App\Model;

class PersonModel
{
    public function __construct(
        public readonly string $id,
        public readonly string $firstName,
        public readonly string $lastName,
        public readonly \DateTimeImmutable $birthdate,
        public readonly string $personalId,
        public readonly \DateTimeImmutable $createdAt,
    ) {
    }
}
