<?php

declare(strict_types=1);

namespace App\Service\Transformer;

use App\Entity\Apartment;
use App\Entity\Person;
use App\Model\EnrichedPersonModel;

class PersonsTransformer
{
    use ApartmentTransformerTrait;

    /**
     * @param Apartment[] $apartments
     *
     * @return EnrichedPersonModel[]
     */
    public function listToEnrichedModels(array $apartments): array
    {
        return array_map(
            fn (Person $apartment) => $this->toEnrichedModel($apartment),
            $apartments,
        );
    }

    public function toEnrichedModel(Person $person): EnrichedPersonModel
    {
        return new EnrichedPersonModel(
            id: $person->getId(),
            firstName: $person->getFirstName(),
            lastName: $person->getLastName(),
            birthdate: $person->getBirthdate(),
            personalId: $person->getPersonalId(),
            apartment: $this->transformApartmentToModel($person->getApartment()),
            createdAt: $person->getCreatedAt(),
        );
    }
}
