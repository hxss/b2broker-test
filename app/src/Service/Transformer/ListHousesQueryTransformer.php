<?php

declare(strict_types=1);

namespace App\Service\Transformer;

use App\Dto\ListHousesDto;
use App\Query\ListHousesQuery;

class ListHousesQueryTransformer
{
    public function toDto(ListHousesQuery $query): ListHousesDto
    {
        return new ListHousesDto(
            page: $query->page,
            perPage: $query->perPage,
        );
    }
}
