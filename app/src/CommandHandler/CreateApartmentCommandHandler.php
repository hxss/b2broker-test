<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Command\CreateApartmentCommand;
use App\Factory\ApartmentFactory;
use App\Service\ApartmentCommandService;
use App\Service\CQRS\CommandHandlerInterface;

class CreateApartmentCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly ApartmentFactory $factory,
        private readonly ApartmentCommandService $service
    ) {
    }

    public function __invoke(CreateApartmentCommand $command): void
    {
        $apartment = $this->factory->create($command->houseId, $command->number);

        $this->service->add($apartment);
    }
}
