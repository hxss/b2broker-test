<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Sequentially;

class CreateHouseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'number',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => new Sequentially([
                        new NotBlank(),
                        new Length(['max' => 20]),
                    ]),
                ]
            )
            ->add(
                'street',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => new Sequentially([
                        new NotBlank(),
                        new Length(['max' => 255]),
                    ]),
                ]
            )
        ;
    }
}
