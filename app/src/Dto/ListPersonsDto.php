<?php

declare(strict_types=1);

namespace App\Dto;

class ListPersonsDto
{
    public function __construct(
        public readonly int $page,
        public readonly int $perPage,
        public readonly ?string $firstName,
        public readonly ?string $lastName,
        public readonly ?\DateTimeImmutable $birthdate,
        public readonly ?string $personalId,
        public readonly ?string $apartmentNumber,
        public readonly ?string $houseNumber,
        public readonly ?string $houseStreet,
    ) {
    }
}
