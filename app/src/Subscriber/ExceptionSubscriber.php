<?php

declare(strict_types=1);

namespace App\Subscriber;

use App\Exception\ApplicationException;
use App\Exception\Rest\FormValidationFailedException;
use App\Exception\Rest\PublicErrorMessageInterface;
use App\Service\FormErrorTransformer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly FormErrorTransformer $formErrorTransformer
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['convertError', -32],
            ],
        ];
    }

    public function convertError(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        $data = '';
        $httpCode = null;
        $headers = [];

        switch (true) {
            case $exception instanceof FormValidationFailedException:
                $form = $exception->getForm();
                $data = $this->formErrorTransformer->transform($form);
                $httpCode = Response::HTTP_BAD_REQUEST;
                break;
            case $exception instanceof PublicErrorMessageInterface:
                $data = $exception->getPublicMessage();
                $httpCode = Response::HTTP_BAD_REQUEST;
                break;
            case $exception instanceof ApplicationException:
                $httpCode = Response::HTTP_BAD_REQUEST;
                break;

            default:
                $httpCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        $event->setResponse(
            new JsonResponse(
                data: $data,
                status: $httpCode,
                headers: $headers,
            )
        );
    }
}
