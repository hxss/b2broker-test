<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Tests\Support\FunctionalTester;
use Symfony\Component\HttpFoundation\Response;

class ListHousesControllerCest
{
    const URL = '/house';

    public function successListTest(FunctionalTester $I)
    {
        // arrange
        $id = $I->getFaker()->uuid();
        $number = (string)$I->getFaker()->numberBetween(1, 50);
        $street = $I->getFaker()->streetName();

        $I->createHouse(
            id: $id,
            number: $number,
            street: $street,
        );

        // act
        $I->sendGet(
            self::URL,
            [
                'page' => 1,
                'perPage' => 1,
            ]
        );

        // assert
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $models = $I->grabResponseJson();
        $I->assertCount(1, $models);
        $model = $models[0];
        $I->assertSame($id, $model['id']);
        $I->assertSame($number, $model['number']);
        $I->assertSame($street, $model['street']);
    }
}
