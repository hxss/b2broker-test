<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Tests\Support\FunctionalTester;
use Symfony\Component\HttpFoundation\Response;

class ListPersonsControllerCest
{
    const URL = '/person';

    public function successListTest(FunctionalTester $I)
    {
        // arrange
        $house = $I->createHouse();
        $apartment = $I->createApartment(house: $house);
        $person = $I->createPerson(apartment: $apartment);

        // act
        $I->sendGet(
            self::URL,
            [
                'page' => 1,
                'perPage' => 1,
                'firstName' => $person->getFirstName(),
                'lastName' => $person->getLastName(),
                'birthdate' => $person->getBirthdate()->format('Y-m-d'),
                'personalId' => $person->getPersonalId(),
                'apartmentNumber' => $apartment->getNumber(),
                'houseNumber' => $house->getNumber(),
                'houseStreet' => $house->getStreet(),
            ]
        );

        // assert
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $models = $I->grabResponseJson();
        $I->assertCount(1, $models);
        $model = $models[0];
        $I->assertSame($person->getId(), $model['id']);
        $I->assertSame($apartment->getId(), $model['apartment']['id']);
        $I->assertSame($house->getId(), $model['apartment']['house']['id']);
    }
}
