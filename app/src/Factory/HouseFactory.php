<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\House;
use Symfony\Component\Uid\Uuid;

class HouseFactory
{
    public function create(string $number, string $street): House
    {
        $house = new House();
        $house
            ->setId((string)Uuid::v4())
            ->setNumber($number)
            ->setStreet($street);

        return $house;
    }
}
