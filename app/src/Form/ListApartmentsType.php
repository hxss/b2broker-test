<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ListApartmentsType extends AbstractType
{
    use PaginationTypeTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'number',
                TextType::class,
            )
            ->add(
                'houseNumber',
                TextType::class,
            )
            ->add(
                'houseStreet',
                TextType::class,
            )
            ->add(
                'personFirstName',
                TextType::class,
            )
            ->add(
                'personLastName',
                TextType::class,
            )
        ;

        $this->buildPaginationForm($builder);
    }
}
