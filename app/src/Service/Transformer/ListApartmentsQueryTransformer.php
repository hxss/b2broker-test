<?php

declare(strict_types=1);

namespace App\Service\Transformer;

use App\Dto\ListApartmentsDto;
use App\Query\ListApartmentsQuery;

class ListApartmentsQueryTransformer
{
    public function toDto(ListApartmentsQuery $query): ListApartmentsDto
    {
        return new ListApartmentsDto(
            page: $query->page,
            perPage: $query->perPage,
            number: $query->number,
            houseNumber: $query->houseNumber,
            houseStreet: $query->houseStreet,
            personFirstName: $query->personFirstName,
            personLastName: $query->personLastName,
        );
    }
}
