<?php

declare(strict_types=1);

namespace App\Dto;

class ListApartmentsDto
{
    public function __construct(
        public readonly int $page,
        public readonly int $perPage,
        public readonly ?string $number,
        public readonly ?string $houseNumber,
        public readonly ?string $houseStreet,
        public readonly ?string $personFirstName,
        public readonly ?string $personLastName,
    ) {
    }
}
