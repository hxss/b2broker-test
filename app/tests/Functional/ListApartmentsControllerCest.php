<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Tests\Support\FunctionalTester;
use Symfony\Component\HttpFoundation\Response;

class ListApartmentsControllerCest
{
    const URL = '/apartment';

    public function successListTest(FunctionalTester $I)
    {
        // arrange
        $house = $I->createHouse();
        $apartment = $I->createApartment(house: $house);
        $person = $I->createPerson(apartment: $apartment);

        // act
        $I->sendGet(
            self::URL,
            [
                'page' => 1,
                'perPage' => 1,
                'number' => $apartment->getNumber(),
                'houseNumber' => $house->getNumber(),
                'houseStreet' => $house->getStreet(),
                'personFirstName' => $person->getFirstName(),
                'personLastName' => $person->getLastName(),
            ]
        );

        // assert
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $models = $I->grabResponseJson();
        $I->assertCount(1, $models);
        $model = $models[0];
        $I->assertSame($apartment->getId(), $model['id']);
        $I->assertSame($house->getId(), $model['house']['id']);
        $I->assertSame($person->getId(), $model['persons'][0]['id']);
    }
}
