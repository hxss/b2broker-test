<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ListHousesType extends AbstractType
{
    use PaginationTypeTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->buildPaginationForm($builder);
    }
}
