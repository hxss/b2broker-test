<?php

declare(strict_types=1);

namespace App\Model;

class ApartmentModel
{
    public function __construct(
        public readonly string $id,
        public readonly string $number,
        public readonly HouseModel $house,
        public readonly \DateTimeImmutable $createdAt,
    ) {
    }
}
