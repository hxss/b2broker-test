<?php

declare(strict_types=1);

namespace App\Service\Transformer;

use App\Dto\ListPersonsDto;
use App\Query\ListPersonsQuery;

class ListPersonsQueryTransformer
{
    public function toDto(ListPersonsQuery $query): ListPersonsDto
    {
        return new ListPersonsDto(
            page: $query->page,
            perPage: $query->perPage,
            firstName: $query->firstName,
            lastName: $query->lastName,
            birthdate: $query->birthdate,
            personalId: $query->personalId,
            apartmentNumber: $query->apartmentNumber,
            houseNumber: $query->houseNumber,
            houseStreet: $query->houseStreet,
        );
    }
}
