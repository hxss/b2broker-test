<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\House;
use App\Exception\Rest\EntityNotFoundException;
use App\Factory\ApartmentFactory;
use App\Repository\HouseRepository;
use App\Tests\Support\UnitTester;
use Codeception\Stub;
use Faker\Factory;
use Faker\Generator;

class ApartmentFactoryCest
{
    private Generator $faker;

    public function _before(UnitTester $I)
    {
        $this->faker = Factory::create();
    }

    public function failFindHouseTest(UnitTester $I)
    {
        // arrange
        $houseRepository = Stub::make(HouseRepository::class, ['find' => null]);
        $factory = new ApartmentFactory($houseRepository);
        $houseId = $this->faker->uuid();

        // act // assert
        $I->expectThrowable(
            new EntityNotFoundException(House::class, $houseId),
            fn () => $factory->create($houseId, '1')
        );
    }

    public function successCreateTest(UnitTester $I)
    {
        // arrange
        $house = new House();
        $houseRepository = Stub::make(HouseRepository::class, ['find' => $house]);
        $factory = new ApartmentFactory($houseRepository);

        $number = '1';

        // act
        $apartment = $factory->create($this->faker->uuid(), $number);

        // assert
        $I->assertRegExp('/^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/', $apartment->getId());
        $I->assertSame($number, $apartment->getNumber());
        $I->assertSame($house, $apartment->getHouse());
    }
}
