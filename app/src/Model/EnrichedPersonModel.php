<?php

declare(strict_types=1);

namespace App\Model;

class EnrichedPersonModel
{
    /**
     * @param PersonModel[] $persons
     */
    public function __construct(
        public readonly string $id,
        public readonly ?string $firstName,
        public readonly ?string $lastName,
        public readonly ?\DateTimeImmutable $birthdate,
        public readonly ?string $personalId,
        public readonly ApartmentModel $apartment,
        public readonly \DateTimeImmutable $createdAt,
    ) {
    }
}
