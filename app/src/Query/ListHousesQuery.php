<?php

declare(strict_types=1);

namespace App\Query;

use App\Service\CQRS\QueryInterface;

class ListHousesQuery implements QueryInterface
{
    public function __construct(
        public readonly int $page,
        public readonly int $perPage,
    ) {
    }
}