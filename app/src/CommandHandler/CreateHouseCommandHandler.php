<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Service\HouseCommandService;
use App\Service\CQRS\CommandHandlerInterface;
use App\Command\CreateHouseCommand;
use App\Factory\HouseFactory;

class CreateHouseCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly HouseFactory $factory,
        private readonly HouseCommandService $service
    ) {
    }

    public function __invoke(CreateHouseCommand $command): void
    {
        $house = $this->factory->create($command->number, $command->street);

        $this->service->add($house);
    }
}
