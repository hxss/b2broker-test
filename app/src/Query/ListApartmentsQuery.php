<?php

declare(strict_types=1);

namespace App\Query;

use App\Service\CQRS\QueryInterface;

class ListApartmentsQuery implements QueryInterface
{
    public function __construct(
        public readonly int $page,
        public readonly int $perPage,
        public readonly ?string $number = null,
        public readonly ?string $houseNumber = null,
        public readonly ?string $houseStreet = null,
        public readonly ?string $personFirstName = null,
        public readonly ?string $personLastName = null,
    ) {
    }
}