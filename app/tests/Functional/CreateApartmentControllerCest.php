<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Entity\Apartment;
use App\Tests\Support\FunctionalTester;
use Codeception\Example;
use Symfony\Component\HttpFoundation\Response;

class CreateApartmentControllerCest
{
    const URL = '/apartment';

    /**
     * @dataProvider invalidRequestProvider
     */
    public function invalidRequestTest(FunctionalTester $I, Example $data)
    {
        // act
        $I->sendPost(self::URL, $data['request']);

        // assert
        $I->seeResponseCodeIs(Response::HTTP_BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->assertSame($data['response'], $I->grabResponseJson());
    }

    public function successCreateTest(FunctionalTester $I)
    {
        // arrange
        $houseId = $I->createHouse()->getId();
        $number = (string)$I->getFaker()->numberBetween(1, 50);

        // act
        $I->sendPost(
            self::URL,
            [
                'houseId' => $houseId,
                'number' => $number,
            ]
        );

        // assert
        $I->seeResponseCodeIs(Response::HTTP_NO_CONTENT);
        $I->seeInRepository(
            Apartment::class,
            ['house' => $houseId, 'number' => $number]
        );
    }

    protected function invalidRequestProvider(): \Generator
    {
        $houseId = '45c063dc-a937-4ea6-9f9f-6b268bc5d4f1';
        $number = '1';

        yield 'empty' => [
            'request' => [],
            'response' => [
                'houseId' => [
                    'This value should not be blank.',
                ],
                'number' => [
                    'This value should not be blank.',
                ],
            ],
        ];
        yield 'has not name' => [
            'request' => ['houseId' => $houseId],
            'response' => [
                'number' => [
                    'This value should not be blank.',
                ],
            ],
        ];
        yield 'has not houseId' => [
            'request' => ['number' => $number],
            'response' => [
                'houseId' => [
                    'This value should not be blank.',
                ],
            ],
        ];
        yield 'invalid houseId' => [
            'request' => ['houseId' => 'houseId', 'number' => $number],
            'response' => [
                'houseId' => [
                    'This value is not a valid UUID.',
                ],
            ],
        ];
        yield 'not found houseId' => [
            'request' => ['houseId' => $houseId, 'number' => $number],
            'response' => "Entity not found: House::{$houseId}",
        ];
    }
}
