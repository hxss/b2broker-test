# Web App test task

Authorization was removed specifically for easier testing

| day | working hours |
|-----|---|
| 2024-02-04 sun | 2 |
| 2024-02-05 mon | 2 |
| 2024-02-06 tue | 2 |
| 2024-02-07 wed | 1 |
| 2024-02-08 thu | 1 |
| 2024-02-12 mon | 2 |
| 2024-02-13 tue | 2 |
| 2024-02-14 wed | 4 |
| total | 16 |

----
# Installation of Web App

1. ```git clone ...```
2. ```cd b2broker-test```
3. ```docker-compose up --build --force-recreate -d```
4. ```docker exec -it b2broker-test-php-fpm-1 composer install```
4. ```docker exec -it b2broker-test-php-fpm-1 bin/console doctrine:migrations:migrate```
5. ```docker exec -it b2broker-test-php-fpm-1 vendor/bin/codecept run```
5. Use ```requests.http``` to test app
---
