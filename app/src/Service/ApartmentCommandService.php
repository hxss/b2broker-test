<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Apartment;
use Doctrine\ORM\EntityManagerInterface;

class ApartmentCommandService
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function add(Apartment $apartment): void
    {
        $this->em->persist($apartment);
        $this->em->flush();
    }

    public function delete(Apartment $apartment): void
    {
        $this->em->remove($apartment);
        $this->em->flush();
    }

    public function update(Apartment $apartment): void
    {
        $this->em->persist($apartment);
        $this->em->flush();
    }

}