<?php

declare(strict_types=1);

namespace App\Service\Transformer;

use App\Entity\Apartment;
use App\Model\EnrichedApartmentModel;

class ApartmentsTransformer
{
    use HouseTransformerTrait;
    use PersonTransformerTrait;

    /**
     * @param Apartment[] $apartments
     *
     * @return EnrichedApartmentModel[]
     */
    public function listToEnrichedModels(array $apartments): array
    {
        return array_map(
            fn (Apartment $apartment) => $this->toEnrichedModel($apartment),
            $apartments,
        );
    }

    public function toEnrichedModel(Apartment $apartment): EnrichedApartmentModel
    {
        return new EnrichedApartmentModel(
            id: $apartment->getId(),
            number: $apartment->getNumber(),
            house: $this->transformHouseToModel($apartment->getHouse()),
            persons: $this->transformPersonsListToModel($apartment->getPersons()->toArray()),
            createdAt: $apartment->getCreatedAt(),
        );
    }
}
