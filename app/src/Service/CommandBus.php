<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\ApplicationException;
use App\Service\CQRS\CommandBusInterface;
use App\Service\CQRS\CommandInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

class CommandBus implements CommandBusInterface
{
    public function __construct(
        private readonly MessageBusInterface $commandBus
    ) {
    }

    public function dispatch(CommandInterface $command): void
    {
        try {
            $this->commandBus->dispatch($command);
        } catch (HandlerFailedException $e) {
            throw $e->getPrevious() instanceof ApplicationException
                ? $e->getPrevious()
                : $e;
        }
    }
}
