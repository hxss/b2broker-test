<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Constraints\Uuid;

class CreatePersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'apartmentId',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => new Sequentially([
                        new NotBlank(),
                        new Uuid(),
                    ]),
                ]
            )
            ->add(
                'firstName',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => new NotBlank(),
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => new NotBlank(),
                ]
            )
            ->add(
                'birthdate',
                DateType::class,
                [
                    'required' => true,
                    'constraints' => new NotBlank(),
                ]
            )
            ->add(
                'personalId',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => new NotBlank(),
                ]
            )
        ;
    }
}
