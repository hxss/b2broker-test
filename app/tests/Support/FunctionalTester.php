<?php

declare(strict_types=1);

namespace App\Tests\Support;

use App\Entity\Apartment;
use App\Entity\House;
use App\Entity\Person;
use Faker\Factory;
use Faker\Generator;

/**
 * Inherited Methods
 *
 * @method void wantTo($text)
 * @method void wantToTest($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause($vars = [])
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;

    /**
     * @return mixed
     */
    public function grabResponseJson()
    {
        return json_decode($this->grabResponse(), true, 512, JSON_THROW_ON_ERROR);
    }

    public function createPerson(
        string $id = null,
        string|Apartment $apartment = null,
        string $firstName = null,
        string $lastName = null,
        \DateTimeImmutable $birthdate = null,
        string $personalId = null,
    ): Person {
        $apartment = match (gettype($apartment)) {
            'object' => $apartment,
            'string' => $this->grabApartmentFromRepository($id),
            'NULL' => $this->createHouse(),
        };

        $id = $this->haveInRepository(
            Person::class,
            [
                'id' => $id ?? $this->getFaker()->uuid(),
                'apartment' => $apartment,
                'firstName' => $firstName ?? $this->getFaker()->firstName(),
                'lastName' => $lastName ?? $this->getFaker()->lastName(),
                'birthdate' => $birthdate ?? new \DateTimeImmutable(),
                'personalId' => $personalId ?? $this->getFaker()->uuid(),
            ]
        );

        return $this->grabPersonFromRepository($id);
    }

    public function grabPersonFromRepository(string $id): Person
    {
        return $this->grabEntityFromRepository(Person::class, ['id' => $id]);
    }

    public function createApartment(
        string $id = null,
        string|House $house = null,
        string $number = null,
    ): Apartment {
        $house = match (gettype($house)) {
            'object' => $house,
            'string' => $this->grabHouseFromRepository($id),
            'NULL' => $this->createHouse(),
        };

        $id = $this->haveInRepository(
            Apartment::class,
            [
                'id' => $id ?? $this->getFaker()->uuid(),
                'house' => $house,
                'number' => $number
                    ?? (string)$this->getFaker()->numberBetween(1, 50),
            ]
        );

        return $this->grabApartmentFromRepository($id);
    }

    public function grabApartmentFromRepository(string $id): Apartment
    {
        return $this->grabEntityFromRepository(Apartment::class, ['id' => $id]);
    }

    public function createHouse(
        string $id = null,
        string $number = null,
        string $street = null,
    ): House {
        $id = $this->haveInRepository(
            House::class,
            [
                'id' => $id ?? $this->getFaker()->uuid(),
                'number' => $number
                    ?? (string)$this->getFaker()->numberBetween(1, 50),
                'street' => $street ?? $this->getFaker()->streetName(),
            ]
        );

        return $this->grabHouseFromRepository($id);
    }

    public function grabHouseFromRepository(string $id): House
    {
        return $this->grabEntityFromRepository(House::class, ['id' => $id]);
    }

    public function getFaker(): Generator
    {
        static $faker = Factory::create();

        return $faker;
    }
}
