CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

ALTER ROLE b2broker_test WITH SUPERUSER CREATEDB CREATEROLE REPLICATION BYPASSRLS;