<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;

class PersonCommandService
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function add(Person $person): void
    {
        $this->em->persist($person);
        $this->em->flush();
    }

    public function delete(Person $person): void
    {
        $this->em->remove($person);
        $this->em->flush();
    }

    public function update(Person $person): void
    {
        $this->em->persist($person);
        $this->em->flush();
    }

}