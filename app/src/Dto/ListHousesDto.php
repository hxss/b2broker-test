<?php

declare(strict_types=1);

namespace App\Dto;

class ListHousesDto
{
    public function __construct(
        public readonly int $page,
        public readonly int $perPage,
    ) {
    }
}
