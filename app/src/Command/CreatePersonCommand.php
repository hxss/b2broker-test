<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\CQRS\CommandInterface;

class CreatePersonCommand implements CommandInterface
{
    public function __construct(
        public readonly string $apartmentId,
        public readonly string $firstName,
        public readonly string $lastName,
        public readonly \DateTimeImmutable $birthdate,
        public readonly string $personalId,
    ) {
    }
}