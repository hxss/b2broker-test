<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Apartment;
use App\Entity\House;
use App\Exception\Rest\EntityNotFoundException;
use App\Repository\HouseRepository;
use Symfony\Component\Uid\Uuid;

class ApartmentFactory
{
    public function __construct(private readonly HouseRepository $houseRepository)
    {
    }

    public function create(string $houseId, string $number): Apartment
    {
        $house = $this->houseRepository->find($houseId);
        if ($house === null) {
            throw new EntityNotFoundException(House::class, $houseId);
        }

        $apartment = new Apartment();
        $apartment
            ->setId((string)Uuid::v4())
            ->setHouse($house)
            ->setNumber($number);

        return $apartment;
    }
}
