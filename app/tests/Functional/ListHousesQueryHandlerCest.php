<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Model\HouseModel;
use App\Query\ListHousesQuery;
use App\QueryHandler\ListHousesQueryHandler;
use App\Tests\Support\FunctionalTester;

class ListHousesQueryHandlerCest
{
    private ListHousesQueryHandler $handler;

    public function _before(FunctionalTester $I)
    {
        $this->handler = $I->grabService(ListHousesQueryHandler::class);
    }

    public function invokeTest(FunctionalTester $I)
    {
        // arrange
        $house = $I->createHouse();
        $query = new ListHousesQuery(page: 1, perPage: 1);

        // act
        $models = ($this->handler)($query);

        // assert
        $I->assertCount(1, $models);
        $model = $models[0];
        $I->assertInstanceOf(HouseModel::class, $model);
        $I->assertSame($house->getId(), $model->id);
        $I->assertSame($house->getNumber(), $model->number);
        $I->assertSame($house->getStreet(), $model->street);
    }
}
