<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Command\CreateApartmentCommand;
use App\CommandHandler\CreateApartmentCommandHandler;
use App\Entity\Apartment;
use App\Tests\Support\FunctionalTester;

class CreateApartmentCommandHandlerCest
{
    private CreateApartmentCommandHandler $handler;

    public function _before(FunctionalTester $I)
    {
        $this->handler = $I->grabService(CreateApartmentCommandHandler::class);
    }

    public function invokeTest(FunctionalTester $I)
    {
        // arrange
        $houseId = $I->createHouse()->getId();
        $number = (string)$I->getFaker()->numberBetween(1, 50);
        $command = new CreateApartmentCommand($houseId, $number);

        // act
        ($this->handler)($command);

        // assert
        $I->seeInRepository(
            Apartment::class,
            ['house' => $houseId, 'number' => $number],
        );
    }
}
