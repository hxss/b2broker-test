<?php

declare(strict_types=1);

namespace App\Service\Transformer;

use App\Entity\Apartment;
use App\Model\ApartmentModel;

trait ApartmentTransformerTrait
{
    use HouseTransformerTrait;

    public function transformApartmentToModel(Apartment $apartment): ApartmentModel
    {
        return new ApartmentModel(
            id: $apartment->getId(),
            number: $apartment->getNumber(),
            house: $this->transformHouseToModel($apartment->getHouse()),
            createdAt: $apartment->getCreatedAt(),
        );
    }
}
