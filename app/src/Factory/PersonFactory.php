<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Apartment;
use App\Entity\Person;
use App\Exception\Rest\EntityNotFoundException;
use App\Repository\ApartmentRepository;
use Symfony\Component\Uid\Uuid;

class PersonFactory
{
    public function __construct(private readonly ApartmentRepository $apartmentRepository)
    {
    }

    public function create(
        string $apartmentId,
        string $firstName,
        string $lastName,
        \DateTimeImmutable $birthdate,
        string $personalId,
    ): Person {
        $apartment = $this->apartmentRepository->find($apartmentId);
        if ($apartment === null) {
            throw new EntityNotFoundException(Apartment::class, $apartmentId);
        }

        $person = new Person();
        $person
            ->setId((string)Uuid::v4())
            ->setApartment($apartment)
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setBirthdate($birthdate)
            ->setPersonalId($personalId)
        ;

        return $person;
    }
}
