<?php

declare(strict_types=1);

namespace App\QueryBuilder;

use Doctrine\ORM\QueryBuilder;

class OffsetPaginationBuilder
{
    public static function buildByQB(
        QueryBuilder $qb,
        int $page,
        int $perPage,
    ): QueryBuilder {
        return $qb
            ->setFirstResult($perPage * ($page - 1))
            ->setMaxResults($perPage);
    }
}
