<?php

declare(strict_types=1);

namespace App\Controller;

use App\Command\CreateApartmentCommand;
use App\Form\CreateApartmentType;
use App\Service\CQRS\CommandBusInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CreateApartmentController extends AbstractBaseController
{
    public function __construct(
        private readonly CommandBusInterface $commandBus
    ) {
    }

    #[Route('/apartment', methods: ['POST'])]
    public function handle(Request $request): Response
    {
        $command = $this->validateRequestAndBuildAction(
            $request,
            CreateApartmentType::class,
            CreateApartmentCommand::class
        );

        $this->commandBus->dispatch($command);

        return new Response(status: Response::HTTP_NO_CONTENT);
    }
}