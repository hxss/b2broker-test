<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\ListPersonsType;
use App\Query\ListPersonsQuery;
use App\Service\CQRS\QueryBusInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ListPersonsController extends AbstractBaseController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/person', methods: ['GET'])]
    public function handle(Request $request): Response
    {
        $query = $this->validateRequestAndBuildAction(
            $request,
            ListPersonsType::class,
            ListPersonsQuery::class
        );

        $response = $this->queryBus->dispatch($query);

        return $this->json($response);
    }
}