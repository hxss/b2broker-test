<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Model\EnrichedApartmentModel;
use App\Model\HouseModel;
use App\Model\PersonModel;
use App\Query\ListApartmentsQuery;
use App\QueryHandler\ListApartmentsQueryHandler;
use App\Tests\Support\FunctionalTester;

class ListApartmentsQueryHandlerCest
{
    private ListApartmentsQueryHandler $handler;

    public function _before(FunctionalTester $I)
    {
        $this->handler = $I->grabService(ListApartmentsQueryHandler::class);
    }

    public function invokeTest(FunctionalTester $I)
    {
        // arrange
        $house = $I->createHouse();
        $apartment = $I->createApartment(house: $house);
        $person = $I->createPerson(apartment: $apartment);

        $query = new ListApartmentsQuery(
            page: 1,
            perPage: 1,
            number: $apartment->getNumber(),
            houseNumber: $house->getNumber(),
            houseStreet: $house->getStreet(),
            personFirstName: $person->getFirstName(),
            personLastName: $person->getLastName(),
        );

        // act
        $models = ($this->handler)($query);

        // assert
        $I->assertCount(1, $models);
        $model = $models[0];
        $I->assertInstanceOf(EnrichedApartmentModel::class, $model);
        $I->assertSame($apartment->getId(), $model->id);
        $I->assertInstanceOf(HouseModel::class, $model->house);
        $I->assertCount(1, $model->persons);
        $I->assertInstanceOf(PersonModel::class, $model->persons[0]);
    }
}
