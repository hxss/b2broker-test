<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\ListApartmentsType;
use App\Query\ListApartmentsQuery;
use App\Service\CQRS\QueryBusInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ListApartmentsController extends AbstractBaseController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/apartment', methods: ['GET'])]
    public function handle(Request $request): Response
    {
        $query = $this->validateRequestAndBuildAction(
            $request,
            ListApartmentsType::class,
            ListApartmentsQuery::class
        );

        $response = $this->queryBus->dispatch($query);

        return $this->json($response);
    }
}