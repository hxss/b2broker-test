<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Constraints\Uuid;

class CreateApartmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'houseId',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => new Sequentially([
                        new NotBlank(),
                        new Uuid(),
                    ]),
                ]
            )
            ->add(
                'number',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => new Sequentially([
                        new NotBlank(),
                        new Length(['max' => 20]),
                    ]),
                ]
            )
        ;
    }
}
