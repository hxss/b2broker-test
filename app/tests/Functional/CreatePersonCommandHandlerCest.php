<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Command\CreatePersonCommand;
use App\CommandHandler\CreatePersonCommandHandler;
use App\Entity\Person;
use App\Tests\Support\FunctionalTester;

class CreatePersonCommandHandlerCest
{
    private CreatePersonCommandHandler $handler;

    public function _before(FunctionalTester $I)
    {
        $this->handler = $I->grabService(CreatePersonCommandHandler::class);
    }

    public function invokeTest(FunctionalTester $I)
    {
        // arrange
        $apartmentId = $I->createApartment()->getId();

        $personalId = $I->getFaker()->uuid();
        $command = new CreatePersonCommand(
            apartmentId: $apartmentId,
            firstName: $I->getFaker()->firstName(),
            lastName: $I->getFaker()->lastName(),
            birthdate: new \DateTimeImmutable(),
            personalId: $personalId,
        );

        // act
        ($this->handler)($command);

        // assert
        $I->seeInRepository(
            Person::class,
            ['apartment' => $apartmentId, 'personalId' => $personalId],
        );
    }
}
