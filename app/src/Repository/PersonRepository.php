<?php

declare(strict_types=1);

namespace App\Repository;

use App\Dto\ListPersonsDto;
use App\Entity\Person;
use App\QueryBuilder\OffsetPaginationBuilder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Person>
 *
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    /**
     * @return Person[]
     */
    public function findByDto(ListPersonsDto $dto): array
    {
        $qb = OffsetPaginationBuilder
            ::buildByQB(
                $this->createQueryBuilder('p'),
                $dto->page,
                $dto->perPage,
            )
            ->orderBy('p.createdAt', 'DESC');

        if ($dto->firstName !== null) {
            $qb->andWhere('p.firstName LIKE :firstName')
                ->setParameter('firstName', $dto->firstName . '%');
        }

        if ($dto->lastName !== null) {
            $qb->andWhere('p.lastName LIKE :lastName')
                ->setParameter('lastName', $dto->lastName . '%');
        }

        if ($dto->birthdate !== null) {
            $qb->andWhere('p.birthdate = :birthdate')
                ->setParameter('birthdate', $dto->birthdate);
        }

        if ($dto->personalId !== null) {
            $qb->andWhere('p.personalId = :personalId')
                ->setParameter('personalId', $dto->personalId);
        }

        $this->buildByApartmentConditions($qb, $dto);
        $this->buildByHouseConditions($qb, $dto);

        return $qb
            ->getQuery()
            ->getResult();
    }

    private function buildByApartmentConditions(
        QueryBuilder $qb,
        ListPersonsDto $dto
    ): void {
        if ($dto->apartmentNumber === null) {
            return;
        }

        $this->joinApartment($qb);

        $qb
            ->andWhere('a.number = :apartmentNumber')
            ->setParameter('apartmentNumber', $dto->apartmentNumber);
    }

    private function buildByHouseConditions(
        QueryBuilder $qb,
        ListPersonsDto $dto
    ): void {
        if (($dto->houseNumber ?? $dto->houseStreet) === null) {
            return;
        }

        $this->joinApartment($qb);
        $qb->join('a.house', 'h');

        if ($dto->houseNumber !== null) {
            $qb->andWhere('h.number = :houseNumber')
                ->setParameter('houseNumber', $dto->houseNumber);
        }

        if ($dto->houseStreet !== null) {
            $qb->andWhere('h.street LIKE :houseStreet')
                ->setParameter('houseStreet', $dto->houseStreet . '%');
        }
    }

    private function joinApartment(QueryBuilder $qb): void
    {
        $joined = (bool)array_filter(
            $qb->getDQLPart('join')['p'] ?? [],
            fn (Join $join) => $join->getAlias() === 'a',
        );

        if (!$joined) {
            $qb->join('p.apartment', 'a');
        }
    }
}
