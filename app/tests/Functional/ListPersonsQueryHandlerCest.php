<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Model\ApartmentModel;
use App\Model\EnrichedPersonModel;
use App\Model\HouseModel;
use App\Query\ListPersonsQuery;
use App\QueryHandler\ListPersonsQueryHandler;
use App\Tests\Support\FunctionalTester;

class ListPersonsQueryHandlerCest
{
    private ListPersonsQueryHandler $handler;

    public function _before(FunctionalTester $I)
    {
        $this->handler = $I->grabService(ListPersonsQueryHandler::class);
    }

    public function invokeTest(FunctionalTester $I)
    {
        // arrange
        $house = $I->createHouse();
        $apartment = $I->createApartment(house: $house);
        $person = $I->createPerson(apartment: $apartment);

        $query = new ListPersonsQuery(
            page: 1,
            perPage: 1,
            firstName: $person->getFirstName(),
            lastName: $person->getLastName(),
            birthdate: $person->getBirthdate(),
            personalId: $person->getPersonalId(),
            apartmentNumber: $apartment->getNumber(),
            houseNumber: $house->getNumber(),
            houseStreet: $house->getStreet(),
        );

        // act
        $models = ($this->handler)($query);

        // assert
        $I->assertCount(1, $models);
        $model = $models[0];
        $I->assertInstanceOf(EnrichedPersonModel::class, $model);
        $I->assertSame($person->getId(), $model->id);
        $I->assertInstanceOf(ApartmentModel::class, $model->apartment);
        $I->assertInstanceOf(HouseModel::class, $model->apartment->house);
    }
}
