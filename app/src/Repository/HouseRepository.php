<?php

declare(strict_types=1);

namespace App\Repository;

use App\QueryBuilder\OffsetPaginationBuilder;
use App\Dto\ListHousesDto;
use App\Entity\House;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<House>
 *
 * @method House|null find($id, $lockMode = null, $lockVersion = null)
 * @method House|null findOneBy(array $criteria, array $orderBy = null)
 * @method House[]    findAll()
 * @method House[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HouseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, House::class);
    }

    /**
     * @return House[]
     */
    public function findByDto(ListHousesDto $dto): array
    {
        return OffsetPaginationBuilder
            ::buildByQB(
                $this->createQueryBuilder('h'),
                $dto->page,
                $dto->perPage,
            )
            ->orderBy('h.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
