<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ListPersonsType extends AbstractType
{
    use PaginationTypeTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'firstName',
                TextType::class,
            )
            ->add(
                'lastName',
                TextType::class,
            )
            ->add(
                'birthdate',
                DateType::class,
            )
            ->add(
                'personalId',
                TextType::class,
            )
            ->add(
                'apartmentNumber',
                TextType::class,
            )
            ->add(
                'houseNumber',
                TextType::class,
            )
            ->add(
                'houseStreet',
                TextType::class,
            )
        ;

        $this->buildPaginationForm($builder);
    }
}
