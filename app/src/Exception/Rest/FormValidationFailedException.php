<?php

declare(strict_types=1);

namespace App\Exception\Rest;

use App\Exception\ApplicationException;
use Symfony\Component\Form\FormInterface;

class FormValidationFailedException extends ApplicationException implements PublicErrorMessageInterface
{
    public function __construct(private readonly FormInterface $form)
    {
        parent::__construct();
    }

    public function getForm(): FormInterface
    {
        return $this->form;
    }

    public function getPublicMessage(): string
    {
        return '';
    }
}
