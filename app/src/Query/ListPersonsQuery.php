<?php

declare(strict_types=1);

namespace App\Query;

use App\Service\CQRS\QueryInterface;

class ListPersonsQuery implements QueryInterface
{
    public function __construct(
        public readonly int $page,
        public readonly int $perPage,
        public readonly ?string $firstName = null,
        public readonly ?string $lastName = null,
        public readonly ?\DateTimeImmutable $birthdate = null,
        public readonly ?string $personalId = null,
        public readonly ?string $apartmentNumber = null,
        public readonly ?string $houseNumber = null,
        public readonly ?string $houseStreet = null,
    ) {
    }
}