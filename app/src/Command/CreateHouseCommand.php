<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\CQRS\CommandInterface;

class CreateHouseCommand implements CommandInterface
{
    public function __construct(
        public readonly string $number,
        public readonly string $street,
    ) {
    }
}