<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\ListHousesType;
use App\Query\ListHousesQuery;
use App\Service\CQRS\QueryBusInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ListHousesController extends AbstractBaseController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/house', methods: ['GET'])]
    public function handle(Request $request): Response
    {
        $query = $this->validateRequestAndBuildAction(
            $request,
            ListHousesType::class,
            ListHousesQuery::class
        );

        $response = $this->queryBus->dispatch($query);

        return $this->json($response);
    }
}