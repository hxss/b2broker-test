<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\CQRS\CommandInterface;

class CreateApartmentCommand implements CommandInterface
{
    public function __construct(
        public readonly string $houseId,
        public readonly string $number,
    ) {
    }
}