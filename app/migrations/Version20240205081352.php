<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240205081352 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'init db structure';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE "public"."houses" (
                "id" UUid NOT NULL,
                "number" Character Varying( 20 ) NOT NULL,
                "street" Character Varying( 255 ) NOT NULL,
                PRIMARY KEY ( "id" )
            );
        ');

        $this->addSql('
            CREATE TABLE "public"."apartments" (
                "id" UUid NOT NULL,
                "house_id" UUid NOT NULL,
                "number" Character Varying( 20 ) NOT NULL,
                PRIMARY KEY ( "id" )
            );
        ');
        $this->addSql('
            ALTER TABLE "public"."apartments"
                ADD CONSTRAINT "fk_apartments_houses" FOREIGN KEY ( "house_id" )
                REFERENCES "public"."houses" ( "id" ) MATCH FULL
                ON DELETE Cascade
                ON UPDATE Cascade
            ;
        ');

        $this->addSql('
            CREATE TABLE "public"."persons" (
                "id" UUid NOT NULL,
                "apartment_id" UUid NOT NULL,
                "first_name" Character Varying( 255 ) NOT NULL,
                "last_name" Character Varying( 255 ) NOT NULL,
                "birthdate" Date NOT NULL,
                "personal_id" Character Varying( 255 ) NOT NULL,
                PRIMARY KEY ( "id" ),
                CONSTRAINT "unique_persons_personal_id" UNIQUE( "personal_id" )
            );
        ');
        $this->addSql('
            ALTER TABLE "public"."persons"
                ADD CONSTRAINT "fk_persons_apartments" FOREIGN KEY ( "apartment_id" )
                REFERENCES "public"."apartments" ( "id" ) MATCH FULL
                ON DELETE Cascade
                ON UPDATE Cascade
            ;
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS "public"."persons" CASCADE;');
        $this->addSql('DROP TABLE IF EXISTS "public"."apartments" CASCADE;');
        $this->addSql('DROP TABLE IF EXISTS "public"."houses" CASCADE;');
    }
}
