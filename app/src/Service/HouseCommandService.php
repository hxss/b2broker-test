<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\House;
use Doctrine\ORM\EntityManagerInterface;

class HouseCommandService
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function add(House $house): void
    {
        $this->em->persist($house);
        $this->em->flush();
    }

    public function delete(House $house): void
    {
        $this->em->remove($house);
        $this->em->flush();
    }

    public function update(House $house): void
    {
        $this->em->persist($house);
        $this->em->flush();
    }

}