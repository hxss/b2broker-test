<?php

declare(strict_types=1);

namespace App\QueryHandler;

use App\Model\EnrichedApartmentModel;
use App\Query\ListPersonsQuery;
use App\Repository\PersonRepository;
use App\Service\CQRS\QueryHandlerInterface;
use App\Service\Transformer\ListPersonsQueryTransformer;
use App\Service\Transformer\PersonsTransformer;

class ListPersonsQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private readonly PersonRepository $repository,
        private readonly ListPersonsQueryTransformer $queryTransformer,
        private readonly PersonsTransformer $personsTransformer,
    ) {
    }

   /**
    * @return EnrichedApartmentModel[]
    */
    public function __invoke(ListPersonsQuery $query): array
    {
        $apartments = $this->repository->findByDto(
            $this->queryTransformer->toDto($query)
        );

        return $this->personsTransformer->listToEnrichedModels($apartments);
    }
}
