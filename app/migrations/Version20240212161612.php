<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240212161612 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add created_at field in tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE "public"."houses"
                ADD COLUMN "created_at" Timestamp Without Time Zone DEFAULT now() NOT NULL;
        ');
        $this->addSql('
            ALTER TABLE "public"."apartments"
                ADD COLUMN "created_at" Timestamp Without Time Zone DEFAULT now() NOT NULL;
        ');
        $this->addSql('
            ALTER TABLE "public"."persons"
                ADD COLUMN "created_at" Timestamp Without Time Zone DEFAULT now() NOT NULL;
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE "public"."houses" DROP COLUMN "created_at";
        ');
        $this->addSql('
            ALTER TABLE "public"."apartments" DROP COLUMN "created_at";
        ');
        $this->addSql('
            ALTER TABLE "public"."persons" DROP COLUMN "created_at";
        ');
    }
}
