<?php

declare(strict_types=1);

namespace App\Exception\Rest;

interface PublicErrorMessageInterface
{
    public function getPublicMessage(): string;
}
