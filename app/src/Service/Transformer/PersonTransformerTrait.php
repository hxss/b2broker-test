<?php

declare(strict_types=1);

namespace App\Service\Transformer;

use App\Entity\Person;
use App\Model\PersonModel;

trait PersonTransformerTrait
{
    /**
     * @param Person[] $persons
     *
     * @return PersonModel[]
     */
    public function transformPersonsListToModel(array $persons): array
    {
        return array_map(
            fn (Person $person) => $this->transformPersonToModel($person),
            $persons,
        );
    }

    public function transformPersonToModel(Person $person): PersonModel
    {
        return new PersonModel(
            id: $person->getId(),
            firstName: $person->getFirstName(),
            lastName: $person->getLastName(),
            birthdate: $person->getBirthdate(),
            personalId: $person->getPersonalId(),
            createdAt: $person->getCreatedAt(),
        );
    }
}
