<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Command\CreatePersonCommand;
use App\Factory\PersonFactory;
use App\Service\CQRS\CommandHandlerInterface;
use App\Service\PersonCommandService;

class CreatePersonCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly PersonFactory $factory,
        private readonly PersonCommandService $service
    ) {
    }

    public function __invoke(CreatePersonCommand $command): void
    {
        $person = $this->factory->create(
            $command->apartmentId,
            $command->firstName,
            $command->lastName,
            $command->birthdate,
            $command->personalId,
        );

        $this->service->add($person);
    }
}
