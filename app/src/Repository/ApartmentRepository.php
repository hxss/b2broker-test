<?php

declare(strict_types=1);

namespace App\Repository;

use App\Dto\ListApartmentsDto;
use App\Entity\Apartment;
use App\QueryBuilder\OffsetPaginationBuilder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Apartment>
 *
 * @method Apartment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Apartment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Apartment[]    findAll()
 * @method Apartment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApartmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Apartment::class);
    }

    /**
     * @return Apartment[]
     */
    public function findByDto(ListApartmentsDto $dto): array
    {
        $qb = OffsetPaginationBuilder
            ::buildByQB(
                $this->createQueryBuilder('a'),
                $dto->page,
                $dto->perPage,
            )
            ->orderBy('a.createdAt', 'DESC');

        if ($dto->number !== null) {
            $qb->andWhere('a.number = :number')
                ->setParameter('number', $dto->number);
        }

        $this->buildByHouseConditions($qb, $dto);
        $this->buildByPersonConditions($qb, $dto);

        return $qb
            ->getQuery()
            ->getResult();
    }

    private function buildByHouseConditions(
        QueryBuilder $qb,
        ListApartmentsDto $dto
    ): void {
        if (($dto->houseNumber ?? $dto->houseStreet) === null) {
            return;
        }

        $qb->join('a.house', 'h');

        if ($dto->houseNumber !== null) {
            $qb->andWhere('h.number = :houseNumber')
                ->setParameter('houseNumber', $dto->houseNumber);
        }

        if ($dto->houseStreet !== null) {
            $qb->andWhere('h.street LIKE :houseStreet')
                ->setParameter('houseStreet', $dto->houseStreet . '%');
        }
    }

    private function buildByPersonConditions(
        QueryBuilder $qb,
        ListApartmentsDto $dto
    ): void {
        if (($dto->personFirstName ?? $dto->personLastName) === null) {
            return;
        }

        $qb->join('a.persons', 'p');

        if ($dto->personFirstName !== null) {
            $qb->andWhere('p.firstName LIKE :personFirstName')
                ->setParameter('personFirstName', $dto->personFirstName . '%');
        }

        if ($dto->personLastName !== null) {
            $qb->andWhere('p.lastName LIKE :personLastName')
                ->setParameter('personLastName', $dto->personLastName . '%');
        }
    }
}
