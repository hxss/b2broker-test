<?php

declare(strict_types=1);

namespace App\Service;

use App\Service\CQRS\QueryBusInterface;
use App\Service\CQRS\QueryInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class QueryBus implements QueryBusInterface
{
    use HandleTrait {
        handle as handleQuery;
    }

    public function __construct(MessageBusInterface $queryBus)
    {
        $this->messageBus = $queryBus;
    }

    /**
     * @return mixed
     */
    public function dispatch(QueryInterface $query)
    {
        return $this->handleQuery($query);
    }
}
