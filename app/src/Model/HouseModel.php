<?php

declare(strict_types=1);

namespace App\Model;

class HouseModel
{
    public function __construct(
        public readonly string $id,
        public readonly string $number,
        public readonly string $street,
        public readonly \DateTimeImmutable $createdAt,
    ) {
    }
}
