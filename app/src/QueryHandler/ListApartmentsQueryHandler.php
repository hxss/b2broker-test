<?php

declare(strict_types=1);

namespace App\QueryHandler;

use App\Model\EnrichedApartmentModel;
use App\Query\ListApartmentsQuery;
use App\Repository\ApartmentRepository;
use App\Service\CQRS\QueryHandlerInterface;
use App\Service\Transformer\ApartmentsTransformer;
use App\Service\Transformer\ListApartmentsQueryTransformer;

class ListApartmentsQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private readonly ApartmentRepository $repository,
        private readonly ListApartmentsQueryTransformer $queryTransformer,
        private readonly ApartmentsTransformer $apartmentsTransformer,
    ) {
    }

   /**
    * @return EnrichedApartmentModel[]
    */
    public function __invoke(ListApartmentsQuery $query): array
    {
        $apartments = $this->repository->findByDto(
            $this->queryTransformer->toDto($query)
        );

        return $this->apartmentsTransformer->listToEnrichedModels($apartments);
    }
}
