<?php

declare(strict_types=1);

namespace App\Service\Transformer;

use App\Entity\House;
use App\Model\HouseModel;

class HousesTransformer
{
    use HouseTransformerTrait;

    /**
     * @param House[] $houses
     *
     * @return HouseModel[]
     */
    public function listToModels(array $houses): array
    {
        return array_map(
            fn (House $house) => $this->transformHouseToModel($house),
            $houses,
        );
    }
}
