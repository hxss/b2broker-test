<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Factory\HouseFactory;
use App\Tests\Support\UnitTester;

class HouseFactoryCest
{

    private HouseFactory $factory;

    public function _before(UnitTester $I)
    {
        $this->factory = new HouseFactory();
    }

    public function createTest(UnitTester $I)
    {
        // arrange
        $number = '1';
        $street = 'first street';

        // act
        $house = $this->factory->create($number, $street);

        // assert
        $I->assertRegExp('/^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/', $house->getId());
        $I->assertSame($number, $house->getNumber());
        $I->assertSame($street, $house->getStreet());
    }
}
