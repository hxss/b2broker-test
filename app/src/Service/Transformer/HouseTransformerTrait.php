<?php

declare(strict_types=1);

namespace App\Service\Transformer;

use App\Entity\House;
use App\Model\HouseModel;

trait HouseTransformerTrait
{
    public function transformHouseToModel(House $house): HouseModel
    {
        return new HouseModel(
            id: $house->getId(),
            number: $house->getNumber(),
            street: $house->getStreet(),
            createdAt: $house->getCreatedAt(),
        );
    }
}
